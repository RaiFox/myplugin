package ru.raifox;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import static org.bukkit.Bukkit.getLogger;

public class MyListener implements Listener {
    @EventHandler
    public void onPlayerDamageDealing(EntityDamageByEntityEvent event) {
        Entity entity = event.getDamager();
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            ItemStack itemInMainHand = player.getInventory().getItemInMainHand();
            if (ItemUpdater.hasAttribute("DAMAGE", itemInMainHand)) {
                String newDamage = ItemUpdater.getAttribute("DAMAGE", itemInMainHand);
                event.setDamage(Double.parseDouble(newDamage));
            }
            player.sendMessage(player.getDisplayName() + " нанес " + event.getDamage() + " урона.");
        }
    }
}