package ru.raifox;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.raifox.exceptions.AttributeMismatchException;
import ru.raifox.exceptions.MetaMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItemUpdater {
    private static Matcher matcher;
    final static String attributeValuePattern = "([+-]?\\d+\\.?\\d*\\-?\\d*\\.?\\d*[%]?)";

    private static int getIndexOfAttribute(final Pattern pattern, final List<String> lore) {
        for (int i = 0; i < lore.size(); i++) {
            matcher = pattern.matcher(lore.get(i));
            if (!matcher.matches()) continue;
            return i;
        }
        return -1;
    }

    private static int getIndexOfAttribute(final String attr, final List<String> lore) {
        return getIndexOfAttribute(Pattern.compile(attr + ": " + attributeValuePattern, Pattern.CASE_INSENSITIVE),
                                   lore);
    }

    public static boolean hasAttribute(final String attr, final ItemStack item) {
        return hasAttribute(Pattern.compile(attr + ": " + attributeValuePattern, Pattern.CASE_INSENSITIVE), item);
    }

    public static boolean hasAttribute(final Pattern pattern, final ItemStack item) {
        return item.hasItemMeta() && item.getItemMeta().hasLore() &&
               hasAttribute(pattern, item.getItemMeta().getLore());
    }

    public static boolean hasAttribute(final Pattern pattern, final List<String> lore) {
        int index = getIndexOfAttribute(pattern, lore);
        return index != -1;
    }

    public static String getAttribute(final String attr, final ItemStack item) {
        return getAttribute(Pattern.compile(attr + ": " + attributeValuePattern, Pattern.CASE_INSENSITIVE), item);
    }

    public static String getAttribute(final Pattern pattern, final ItemStack item) {
        return getAttribute(pattern,
                (item.getItemMeta().hasLore() ? item.getItemMeta().getLore() : new ArrayList<>()));
    }

    public static String getAttribute(final Pattern pattern, final List<String> lore) {
        int index = getIndexOfAttribute(pattern, lore);
        if (index == -1) {
            throw new AttributeMismatchException(pattern);
        }
        matcher = pattern.matcher(lore.get(index));
        matcher.matches();
        return matcher.group(1);
    }

    private static void replaceAttributeValue(final List<String> lore, final String attr, final String value) {
        int index = getIndexOfAttribute(attr, lore);
        if (index == -1) {
            throw new AttributeMismatchException(attr);
        }
        lore.set(index, attr + ": " + value);
    }

    public static ItemStack setAttribute(final ItemStack item, final String attr, final String value) {
        ItemMeta meta = item.getItemMeta();
        List<String> lore = new ArrayList<>();
        if (item.getItemMeta().hasLore())
            lore = meta.getLore();
        if (!hasAttribute(Pattern.compile(attr + ": " + attributeValuePattern, Pattern.CASE_INSENSITIVE), lore)) {
            lore.add(attr + ": " + value);
        } else {
            replaceAttributeValue(lore, attr, value);
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }




}
