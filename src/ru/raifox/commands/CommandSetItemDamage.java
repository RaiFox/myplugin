package ru.raifox.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.raifox.ItemUpdater;
import ru.raifox.exceptions.CommandArgumentsException;

import java.util.ArrayList;
import java.util.List;

public class CommandSetItemDamage implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ItemStack item = player.getInventory().getItemInMainHand();
            try {
                if (item.getType() == null || item.getType() == Material.AIR) {
                    throw new CommandArgumentsException(ChatColor.RED + "Сначала возьмите предмет в руку!");
                }
            } catch (CommandArgumentsException e) {
                player.sendMessage(e.getMessage());
                return true;
            }
            double newDamage = 1;
            try {
                newDamage = Double.parseDouble(args[0]);
                if (1 > newDamage || newDamage > 100) {
                    throw new CommandArgumentsException(ChatColor.RED + "Урон должен быть числом в диапазоне от 0 до 100");
                }
            } catch (CommandArgumentsException | NumberFormatException e) {
                player.sendMessage(ChatColor.RED + "Урон должен быть числом в диапазоне от 0 до 100");
                return true;
            }
            player.getInventory().setItemInMainHand(ItemUpdater.setAttribute(item, "DAMAGE", "" + newDamage));
        }
        return true;
    }
}
