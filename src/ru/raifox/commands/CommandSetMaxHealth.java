package ru.raifox.commands;

import org.bukkit.ChatColor;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.raifox.exceptions.CommandArgumentsException;

public class CommandSetMaxHealth implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            double newMaxHealth = 20;
            try {
                newMaxHealth = Double.parseDouble(args[0]);
                if (1 > newMaxHealth || newMaxHealth > 100) {
                    throw new CommandArgumentsException(ChatColor.RED + "Количество жизней должно быть числом в диапазоне от 1 до 100");
                }
            } catch (CommandArgumentsException | NumberFormatException e) {
                player.sendMessage(ChatColor.RED + "Количество жизней должно быть числом в диапазоне от 1 до 100");
                return true;
            }
            double curHealthProc = player.getHealth() / player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
            player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(newMaxHealth);
            player.setHealth(curHealthProc * newMaxHealth);
        }
        return true;
    }
}
