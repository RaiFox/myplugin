package ru.raifox.exceptions;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

public class MetaMismatchException extends RuntimeException {
    public MetaMismatchException(ItemStack item) {
        super("Item '" + item.getType() + "' не имеет мета-данных");
        Bukkit.getLogger().warning("Item '" + item.getType() + "' не имеет мета-данных");
    }
}
