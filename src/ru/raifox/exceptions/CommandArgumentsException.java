package ru.raifox.exceptions;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandArgumentsException extends RuntimeException {
    public CommandArgumentsException (CommandSender sender, final String message) {
        super(message);
    }
    public CommandArgumentsException (final String message) {
        super(message);
    }
}
