package ru.raifox.exceptions;

import org.bukkit.Bukkit;
import org.bukkit.attribute.AttributeInstance;
import ru.raifox.MyPlugin;

import java.util.List;
import java.util.regex.Pattern;

public class AttributeMismatchException extends RuntimeException {
    public AttributeMismatchException(final String attribute) {
        super("Атрибут '" + attribute + "' не найден!");
        Bukkit.getLogger().warning("Атрибут '" + attribute + "' не найден!");
    }

    public AttributeMismatchException(final Pattern pattern) {
        super("Атрибут '" + pattern.split(":")[0] + "' не найден!");
        Bukkit.getLogger().warning("Атрибут '" + pattern.split(":")[0] + "' не найден!");
    }

    public AttributeMismatchException() {
        super("Атрибут не найден!");
        Bukkit.getLogger().warning("Атрибут не найден!");
    }

}
