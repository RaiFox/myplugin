package ru.raifox;

import org.bukkit.plugin.java.JavaPlugin;
import ru.raifox.commands.CommandSetItemDamage;
import ru.raifox.commands.CommandSetMaxHealth;

public class MyPlugin extends JavaPlugin{
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new MyListener(), this);
        this.getCommand("sethp").setExecutor(new CommandSetMaxHealth());
        this.getCommand("set_item_damage").setExecutor(new CommandSetItemDamage());
    }

    public void onDisable() {}

}
